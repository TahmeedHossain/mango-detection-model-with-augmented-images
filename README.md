# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Plant disease detection model ###

This repository includes the detection model which will help to detect diseases for mango dataset(augmented) with 96% accuracy.
More accuracy is possible with higher epoch/ number of images. 
The model is a CNN model and hyperparameters of the model are:

* EPOCHS = 50
* STEPS = 100
* LR = 1e-3
* BATCH_SIZE = 32
* WIDTH = 128
* HEIGHT = 128
* DEPTH = 3

Train test ratio - .8/.2  

Total class - 6  
 

### Setup ###

Just run the script in Google Colab Notebook. Every process is automated. 


